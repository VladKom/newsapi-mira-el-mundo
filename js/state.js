const state= {
    API_KEY : 'ebd160d40b584abbbd6a5f67b94d6e54',
    source : 'https://newsapi.org/v2/',
    typeSearch: null,
    country : null,
    category: null,
    keyWord : null,
    url : null,
    data : null,
    articles : null,
    date : null ,
    screen : null,
  }  
  
  const notDefinedScreen = document.getElementById("notDefined")  
const startScreen = document.getElementById("startScreen")
const countrySearchScreen = document.getElementById("countrySearchScreen")
const keyWordSearchScreen = document.getElementById("keyWordSearchScreen")
const listNewsScreen = document.getElementById("listNewsScreen")
const articleDetallsScreen = document.getElementById("articleDetallsScreen")
const boxChooseNews = document.getElementById("boxChooseNews")
const img1 = document.getElementById("img1")
const imgAndContent = document.getElementById("imgAndContent")
const buttonCountry = document.getElementById("buttonSearchCountry")
const buttonKeyWord = document.getElementById("buttonSearchKeyWord")
const buttonSearchNews = document.getElementById("buttonSearchNews")  
const buttonNewSearch = document.getElementById("buttonNewSearch")
const buttonGoBack = document.getElementById("buttonGoBack")
  
function initateVariables(state){
    state.articles = null;
    state.typeSearch = null;
    state.country = null;
    state.category = null;
    state.keyWord = null;
    state.url = null;
    state.data = null;
    state.date = null;
    state.screen = null;
    return state;
  } 
  try {module.exports={
    initateVariables
        }} catch{}