function hideAllScreens(){
  startScreen.style.display="none";
  countrySearchScreen.style.display="none";
  keyWordSearchScreen.style.display="none";
  listNewsScreen.style.display="none";
  articleDetallsScreen.style.display="none";
  buttonSearchNews.style.display="none";
  buttonNewSearch.style.display="none";
  buttonGoBack.style.display="none";
  notDefinedScreen.style.display="none";
}
function showSnotDefinedScreen(){
  hideAllScreens()
  notDefinedScreen.style.display="block";
  buttonNewSearch.style.display="block";
 }
function showStartScreen(){
  hideAllScreens()
  startScreen.style.display="block"; 
}
function showScreensearchCountry(){
  hideAllScreens()
  countrySearchScreen.style.display="block";
  buttonSearchNews.style.display="block";
  buttonNewSearch.style.display="block";
}
function showScreenKeyWord(){
  hideAllScreens()
  keyWordSearchScreen.style.display="block";
  buttonSearchNews.style.display="block";
  buttonNewSearch.style.display="block";
}
function showScreenNewsList(){
  hideAllScreens()
  listNewsScreen.style.display="block";
  buttonNewSearch.style.display="block";
  buttonGoBack.style.display="block";
}
function showScreenArticlesDetails(){
  hideAllScreens()
  articleDetallsScreen.style.display="block";
  buttonNewSearch.style.display="block";
  buttonGoBack.style.display="block";
  boxChooseNews.style.display="block";
  imgAndContent.style.display="flex";
  img1.style.display="block";
}
function putArticlesInList(){
  let news = state.data.articles
    for (i = 0; i <news.length; i++) {
      if (news[i].content === null) {
        } else {
          var box = document.createElement("div"); box.setAttribute("id",i) ; box.setAttribute("class","boxNews");
          document.getElementById("listNewsScreen").appendChild(box);
          var titleNews = document.createElement("p"); titleNews.setAttribute("class","titleNews");
          titleNews.innerHTML = " "+news[i].title+" ";
          document.getElementById(i).appendChild(titleNews); 
          var description = document .createElement("p"); description.setAttribute("class","description");
          description.innerHTML =" "+news[i].description+" "; 
          document.getElementById(i).appendChild(description);
          var source = document.createElement("p"); source.setAttribute("class","source");
          source.innerHTML = "<span> Source: </span> "+news[i].source.name+" ";
          document.getElementById(i).appendChild(source);
          var author= document.createElement("p"); author.setAttribute("class","author");
          author.innerHTML = "<span> Author: </span> "+news[i].author+" ";
          document.getElementById(i).appendChild(author);
          var seeMore = document.createElement("button"); box.setAttribute("class","boxNews");
          seeMore.setAttribute("class","buttonSeeMore");
          seeMore.setAttribute("value",i);
          seeMore.innerHTML = "See more"
          document.getElementById(i).appendChild(seeMore);
        }
  
    }
}

function expandArticleInformations(Event){
  showScreenArticlesDetails();
  state.screen = "details"
  newsPosition=Event.target.value
  let news = state.data.articles  
  document.getElementById("source").innerHTML =" "+news[newsPosition].source.name+" ";
  document.getElementById("img1").innerHTML = ' <img src="'+ news[newsPosition].urlToImage+'"> ';
  document.getElementById("title").innerHTML = ""+news[newsPosition].title+" ";
  document.getElementById("content").innerHTML = " "+news[newsPosition].content+" ";
  document.getElementById("author").innerHTML = " "+news[newsPosition].author+" ";
  document.getElementById("date").innerHTML = " "+news[newsPosition].publishedAt+" ";
  document.getElementById("readArticle").innerHTML = "<a> Leer Más </a> ";
  document.getElementById("readArticle").setAttribute("href",""+news[newsPosition].url+"");
  document.getElementById("readArticle").setAttribute("target","_blank");
}
try {module.exports={
hideAllScreens,
showStartScreen,
showScreensearchCountry,
showScreenKeyWord,
showScreenNewsList,
showScreenArticlesDetails,
putArticlesInList,
expandArticleInformations,
    }} catch{}