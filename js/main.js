function changeTypesearchToCountry(){
  state.typeSearch = buttonCountry.value
}
function changeTypesearchToKeyWord(){
  state.typeSearch = buttonKeyWord.value
}   
function generateUrlKeyWord(){
  let urlKeyWord= state.source+"everything?q="+state.keyWord+"&from="+state.date+"&to="+state.date+"&apiKey="+state.API_KEY;
  state.url=urlKeyWord
}  
function generateUrlCountry(){
  let urlCountry=  state.source+"top-headlines?country="+state.country+"&category="+state.category+"&apiKey="+state.API_KEY;
  state.url=urlCountry
}   
function saveChooseCountry(){
  state.country = document.getElementById('countryList').value
}
function saveChooseCategory(){
  state.category = document.getElementById('categoryList').value
}
function saveChooseDate(){
  state.date = document.getElementById('startDate').value
}
function saveChooseKeyWord(){
  let word = document.getElementById('keyWordText').value;
  state.keyWord = encodeURI(word)
}
function determinateAPIUrl (){
  if (state.typeSearch === "country") {
    saveChooseCountry();
    saveChooseCategory();
    generateUrlCountry()
  } else if (state.typeSearch === "keyWord") {
    saveChooseKeyWord()
    saveChooseDate();
    generateUrlKeyWord() 
  } 
}
function capturaButtones(){  
  let arrayButtones = document.getElementsByClassName("buttonSeeMore")
      for (i = 0; i <arrayButtones.length; i++)  {
       arrayButtones[i].addEventListener("click",expandArticleInformations)
      }
}

try {module.exports={
  changeTypesearchToCountry,
  changeTypesearchToKeyWord,
  generateUrlKeyWord,
  generateUrlCountry,
  saveChooseCategory,
  saveChooseDate,
  saveChooseKeyWord,
  determinateAPIUrl ,
  capturaButtones
  }} catch{}

     