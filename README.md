# NewsApi Mira el mundo 
This project : MiraElMundo, is a page that let you read news from all over the world, using a NEWSAPI.
It was designed and created by :
                * Diego ROBLES
                * Vladislav KOMARENKO 
                * Mohamed Yassine AJAMI
for their first project using an API.
The API used for this project is a proprety of : https://newsapi.org

The tools used in this project are : 
     - Code visual studio : writing and executing code
     - Scrum : project management
     - Gitlab
     - Slack : sharing ideas and documentation.
     
The code using in programation respect certain rules :
* Use Git for version history. 
* No PUSH without validation of another member.
* Use easily pronounceable names for variables and methods.
* With variables use the name to show intention. 
* Be consistent. 
* Don’t hesitate to use technical terms in names. 
* Use a verb as the first word in method and use a noun for class. Use camelCase for variable and function name. The class should start from the capital.
* Make functions apparent. Keep a function as short as possible.
* Don’t comment on bad code. CHANGE IT


